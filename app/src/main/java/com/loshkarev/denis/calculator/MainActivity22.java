package com.loshkarev.denis.calculator;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.loshkarev.denis.calculator.databinding.ActivityMainBinding;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity22 extends AppCompatActivity {
    private static final String LOG_TAG = "MainActivity";
    private static final String SAVE_CURRENT_INPUT_TEXT = "SAVE_CURRENT_INPUT_TEXT";
    private static final String SAVE_CURRENT_RESULT = "SAVE_CURRENT_RESULT";
    private static final String SAVE_CURRENT_NUM = "SAVE_CURRENT_NUM";
    private static final String SAVE_CURRENT_MEM = "SAVE_CURRENT_MEM";
    private static final String SAVE_CURRENT_OPERATION = "SAVE_CURRENT_OPERATION";
    private static final String RESULT_SCREEN_START_TEXT = "0.0";

    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.button6)
    Button button6;
    @BindView(R.id.button7)
    Button button7;
    @BindView(R.id.button8)
    Button button8;
    @BindView(R.id.button9)
    Button button9;
    @BindView(R.id.button0)
    Button button0;
    @BindView(R.id.resultScreen)
    TextView resultScreen;
    @BindView(R.id.memoryLable)
    TextView memoryLable;
    @BindView(R.id.memoryResult)
    TextView memoryResult;

    private StringBuilder resultScreenText;
    private Calculator calc;
//    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        Log.d(LOG_TAG, "-------------------------");
        Log.d(LOG_TAG, "Started");
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
//        binding.setActivity(this);
//        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        resultScreenText = new StringBuilder(16);
        calc = Calculator.getInstance();
        clearResultScreen();

        if (savedState != null) {
            resultScreenText.append(savedState.getString(SAVE_CURRENT_INPUT_TEXT));
            calc.setResult(savedState.getFloat(SAVE_CURRENT_RESULT));
            calc.setNum(savedState.getFloat(SAVE_CURRENT_NUM));

            float mem = savedState.getFloat(SAVE_CURRENT_MEM);
            if (mem != 0) {
                calc.setMem(mem);
                showMemoryLable();
            }

            calc.setOperation(Operation.valueOf(savedState.getString(SAVE_CURRENT_OPERATION)));
            setResultScreen(resultScreenText);
            Log.d(LOG_TAG, "Current result restored");
        }

        Log.d(LOG_TAG, "onCreate finished");
    }

    @OnClick({R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
            R.id.button6, R.id.button7, R.id.button8, R.id.button9, R.id.button0})
    protected void clickNumberButtonAction(Button button) {
        CharSequence buttonText = button.getText();
        if (resultScreenText.length() > 1) {
            if (resultScreenText.length() < 15) {
                resultScreenText.append(buttonText);
            }
        } else if (resultScreenText.length() == 1) {
            if (resultScreenText.charAt(0) == '0') {
                resultScreenText.deleteCharAt(0);
                resultScreenText.insert(0, buttonText);
            } else {
                resultScreenText.append(buttonText);
            }
        } else {
            resultScreenText.append(buttonText);
        }
        setResultScreen(resultScreenText);
        Log.d(LOG_TAG, "Button" + buttonText + " pressed");
    }

    @OnClick(R.id.buttonDot)
    protected void clickDot() {
        if (resultScreenText.indexOf(Static.DOT) <= 0) {
            if (resultScreenText.length() > 0) {
                resultScreenText.append(Static.DOT);
                setResultScreen(resultScreenText);
            } else {
                resultScreenText.append("0" + Static.DOT);
                setResultScreen(resultScreenText);
            }
        }
    }

    private void operationClicked() {
        if (resultScreenText.length() > 0) {
            calc.parseTextToNum(resultScreenText.toString());
            initResultTextBuilder();
            setResultScreen(calc.calculate());

        }
    }

    @OnClick(R.id.buttonCalculate)
    protected void calculateAction() {
        if (resultScreenText.length() > 0) {
            calc.parseTextToNum(resultScreenText.toString());
        }
        initResultTextBuilder();
        float res = calc.calculate();
        Log.d(LOG_TAG, "Result = " + res);
        setResultScreen(res);
    }

    @OnClick(R.id.buttonPlus)
    protected void plusAction() {
        operationClicked();
        calc.setOperation(Operation.PLUS);
    }

    @OnClick(R.id.buttonMinus)
    protected void minusAction() {
        operationClicked();
        calc.setOperation(Operation.MINUS);
    }

    @OnClick(R.id.buttonMultiply)
    protected void multiplyAction() {
        operationClicked();
        calc.setOperation(Operation.MULTIPLY);
    }

    @OnClick(R.id.buttonDivide)
    protected void divideAction() {
        operationClicked();
        calc.setOperation(Operation.DIVIDE);
    }

    @OnClick(R.id.buttonChange)
    protected void changeSign() {
        if (resultScreenText.length() > 0) {
            if (resultScreenText.charAt(0) != Static.MINUS) {
                resultScreenText.insert(0, Static.MINUS);
            } else {
                resultScreenText.deleteCharAt(0);
            }
            setResultScreen(resultScreenText);
        } else if (resultScreen.getText().length() > 0
                && calc.getResult() != null
                && calc.getResult() != 0) {
            calc.changeResultSign();
            setResultScreen(calc.getResult());
        }
    }

    @OnClick(R.id.buttonCancel)
    protected void clearAction() {
        clearResultScreen();
        calc.clear();
    }

    @OnClick(R.id.buttonDel)
    protected void deleteAction() {
        if (resultScreenText.length() > 0) {
            resultScreenText.deleteCharAt(resultScreenText.length() - 1);
            if (resultScreenText.length() == 1
                    && resultScreenText.charAt(0) == Static.MINUS) {
                clearResultScreen();
            } else {
                setResultScreen(resultScreenText);
            }
        }
    }

    @OnClick(R.id.buttonMemPlus)
    protected void memPlusAction() {
        calc.memPlus(resultScreen.getText().toString());
        setMemoryScreen(calc.getMem());
        showMemoryLable();
    }

    @OnClick(R.id.buttonMemMinus)
    protected void memMinusAction() {
        calc.memMinus(resultScreen.getText().toString());
        setMemoryScreen(calc.getMem());
        showMemoryLable();
    }

    @OnClick(R.id.buttonMemRead)
    protected void memReadAction() {
        float mem = calc.getMem();
        if (mem != 0) {
            initResultTextBuilder();
            resultScreenText.append(mem);
            setResultScreen(resultScreenText);
        }
    }

    @OnClick(R.id.buttonMemClear)
    protected void memClearAction() {
        calc.memClear();
        hideMemoryLable();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SAVE_CURRENT_INPUT_TEXT, resultScreenText.toString());
        if (calc.getResult() != null) outState.putFloat(SAVE_CURRENT_RESULT, calc.getResult());
        outState.putFloat(SAVE_CURRENT_NUM, calc.getNum());
        outState.putFloat(SAVE_CURRENT_MEM, calc.getMem());
        outState.putString(SAVE_CURRENT_OPERATION, calc.getOperation().toString());
        Log.d(LOG_TAG, "Current result saved to bundle");
    }

    private void initResultTextBuilder() {
        resultScreenText.setLength(0);
        Log.d(LOG_TAG, "Result text builder initiated" + resultScreenText.toString());
    }

    private void setMemoryScreen(String text) {
        memoryResult.setText(text);
    }

    private void setMemoryScreen(Float f) {
        setMemoryScreen(f.toString());
    }

    private void showMemoryLable() {
        memoryResult.setVisibility(View.VISIBLE);
        memoryLable.setVisibility(View.VISIBLE);
    }

    private void hideMemoryLable() {
        memoryResult.setVisibility(View.INVISIBLE);
        memoryLable.setVisibility(View.INVISIBLE);
    }

    private void setResultScreen(String res) {
//        resultScreen.setText(res);
//        binding.notifyChange();
        checkTextSize();
    }

    private void setResultScreen(StringBuilder text) {
        String result = text.toString();
        setResultScreen(result);
    }

    private void setResultScreen(Float f) {
        setResultScreen(f.toString());
    }

    private void checkTextSize() {
        if (resultScreen.getText().length() < 10) {
            resultScreen.setTextSize(60);
        } else {
            resultScreen.setTextSize(40);
        }
    }
    private void clearResultScreen() {
        resultScreen.setText(RESULT_SCREEN_START_TEXT);
        checkTextSize();
        initResultTextBuilder();
    }


    public String getResultScreenText() {
        return resultScreenText.toString();
    }
}
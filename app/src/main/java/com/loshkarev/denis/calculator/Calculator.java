package com.loshkarev.denis.calculator;

/**
 * Created by Denis Loshkarev.
 */
public class Calculator {
    private static Calculator instance;
    private Float result;
    private Float num;
    private Float mem;
    private Operation operation;

    private Calculator() {
        this.operation = Operation.NONE;
        this.num = 0f;
        this.mem = 0f;
    }

    public float calculate() {
        switch (operation) {
            case PLUS:
                plus(num);
                break;
            case MINUS:
                minus(num);
                break;
            case MULTIPLY:
                multiply(num);
                break;
            case DIVIDE:
                divide(num);
                break;
            default:
                if (result == null) return 0f;
                break;
        }
        return result;
    }

    public float parseText(String textNum) {
        return Float.parseFloat(textNum);
    }

    public void parseTextToNum(String textNum) {
        float num = parseText(textNum);
        if (result != null) {
            this.num = num;
        } else {
            this.result = num;
        }
    }

    public void setNum(Float num) {
        this.num = num;
    }

    public Float getNum() {
        return num;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setResult(Float result) {
        this.result = result;
    }

    public Float getResult() {
        return result;
    }

    public void clear() {
        result = null;
        num = 0f;
        operation = Operation.NONE;
    }

    public Float getMem() {
        return mem;
    }

    public void setMem(Float mem) {
        this.mem = mem;
    }

    private void divide(float num) {
        result = result / num;
    }

    private void multiply(float num) {
        result = result * num;
    }

    private void minus(float num) {
        result = result - num;
    }

    private void plus(float num) {
        result = result + num;
    }

    private void memPlus(float num) {
        mem += num;
    }

    public void memPlus(String text) {
        memPlus(parseText(text));
    }

    private void memMinus(float num) {
        mem -= num;
    }

    public void memMinus(String text) {
        memMinus(parseText(text));
    }

    public void memClear() {
        mem = 0f;
    }

    public void changeResultSign() {
        if (result != null) {
            result *= -1;
        }
    }

    public static Calculator getInstance() {
        if (instance == null) instance = new Calculator();
        return instance;
    }
}

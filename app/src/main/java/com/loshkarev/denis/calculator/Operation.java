package com.loshkarev.denis.calculator;

/**
 * Created by Denis Loshkarev.
 */
public enum Operation {
    PLUS, MINUS, MULTIPLY, DIVIDE, NONE
}

package com.loshkarev.denis.calculator;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.loshkarev.denis.calculator.databinding.ActivityMainBinding;

/**
 * Created by Denis Loshkarev.
 */
public class MainActivity extends AppCompatActivity {

    private static final String RESULT_SCREEN_START_TEXT = "0.0";
    private static final String LOG_TAG = "MainActivity";
    private ActivityMainBinding binding;
    private StringBuilder textBuilder = new StringBuilder(16);
    private String resultText;
    private Calculator calc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "-------------------------");
        Log.d(LOG_TAG, "Started");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setActivity(this);
        calc = Calculator.getInstance();
        clearResultScreen();
    }

    public void numberClicked(View view) {
        Button button = (Button) view;
        CharSequence buttonText = button.getText();
        if (textBuilder.length() > 1) {
            if (textBuilder.length() < 15) {
                textBuilder.append(buttonText);
            }
        } else if (textBuilder.length() == 1) {
            if (textBuilder.charAt(0) == '0') {
                textBuilder.deleteCharAt(0);
                textBuilder.insert(0, buttonText);
            } else {
                textBuilder.append(buttonText);
            }
        } else {
            textBuilder.append(buttonText);
        }
        resultText = textBuilder.toString();
        binding.setActivity(this);
        Log.d(LOG_TAG, "Pressed button" + buttonText);
    }
    
    public void clickDot() {
        if (textBuilder.indexOf(Static.DOT) < 0) {
            if (textBuilder.length() > 0) {
                textBuilder.append(Static.DOT);
            } else {
                textBuilder.append("0" + Static.DOT);
            }
            updateText();
        }
    }

    private void initResultTextBuilder() {
        textBuilder.setLength(0);
        Log.d(LOG_TAG, "Result text builder initiated" + textBuilder.toString());
    }

    private void clearResultScreen() {
        initResultTextBuilder();
        resultText = RESULT_SCREEN_START_TEXT;
        binding.setActivity(this);
    }

    public void clearAction() {
        clearResultScreen();
        binding.setActivity(this);
        calc.clear();
    }
    
    public void deleteAction() {
        if (textBuilder.length() > 0) {
            textBuilder.deleteCharAt(textBuilder.length() - 1);
            if (textBuilder.length() == 1
                    && textBuilder.charAt(0) == Static.MINUS) {
                clearResultScreen();
            }
            updateText();
        }
        if (textBuilder.length() == 0) {
            clearResultScreen();
        }
    }

    public void changeSign() {
        if (textBuilder.length() > 0) {
            if (textBuilder.charAt(0) != Static.MINUS) {
                textBuilder.insert(0, Static.MINUS);
            } else {
                textBuilder.deleteCharAt(0);
            }
            updateText();
        } else if (binding.resultScreen.getText().length() > 0
                && calc.getResult() != null
                && calc.getResult() != 0) {
            calc.changeResultSign();
            updateText(calc.getResult());
        }
    }

    private void operationClicked() {
        if (textBuilder.length() > 0) {
            calc.parseTextToNum(textBuilder.toString());
            initResultTextBuilder();
            updateText(calc.calculate());
        }
    }

    public void calculateAction() {
        if (textBuilder.length() > 0) {
            calc.parseTextToNum(textBuilder.toString());
        }
        initResultTextBuilder();
        float res = calc.calculate();
        Log.d(LOG_TAG, "Result = " + res);
        updateText(res);
    }

    public void plusAction() {
        operationClicked();
        calc.setOperation(Operation.PLUS);
    }

    public void minusAction() {
        operationClicked();
        calc.setOperation(Operation.MINUS);
    }

    public void multiplyAction() {
        operationClicked();
        calc.setOperation(Operation.MULTIPLY);
    }

    public void divideAction() {
        operationClicked();
        calc.setOperation(Operation.DIVIDE);
    }

    public void memPlusAction() {
        calc.memPlus(binding.resultScreen.getText().toString());
        initResultTextBuilder();
        setMemoryScreen(calc.getMem());
        showMemoryLable();
    }

    public void memMinusAction() {
        calc.memMinus(binding.resultScreen.getText().toString());
        initResultTextBuilder();
        setMemoryScreen(calc.getMem());
        showMemoryLable();
    }

    public void memReadAction() {
        float mem = calc.getMem();
        if (mem != 0) {
            initResultTextBuilder();
            textBuilder.append(mem);
            updateText();
        }
    }

    public void memClearAction() {
        calc.memClear();
        hideMemoryLable();
    }

    private void setMemoryScreen(String text) {
        binding.memoryResult.setText(text);
    }

    private void setMemoryScreen(Float f) {
        setMemoryScreen(f.toString());
    }

    private void showMemoryLable() {
        binding.memoryResult.setVisibility(View.VISIBLE);
        binding.memoryLable.setVisibility(View.VISIBLE);
    }

    private void hideMemoryLable() {
        binding.memoryResult.setVisibility(View.INVISIBLE);
        binding.memoryLable.setVisibility(View.INVISIBLE);
    }

    private void updateText() {
        resultText = textBuilder.toString();
        binding.setActivity(this);
    }

    private void updateText(String text) {
        resultText = text;
        binding.setActivity(this);
    }

    private void updateText(Float f) {
        updateText(f.toString());
    }

    public String resultText() {
        return resultText;
    }
}
